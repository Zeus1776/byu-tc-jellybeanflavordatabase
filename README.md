# BYU-TC-JellyBeanFlavorDatabase

## Description
A simple web app for listing jelly bean flavors, adding to and removing from the list, and for editing flavors in the database. 

## Installation
To run this, you'll need a MySQL database, which you can find in the Container Registry. The provided container has the needed schema to interact with the code, though the schema is also provided alongside the Java code should it be needed. The username/password for the database is root/J3llyB34n.

## Usage
Keeping track of your favorite/least favorite flavors of jellybeans, or keeping a catalog of the flavors you currently have.

## Authors
It's just me.

## License
The code in this baby is open source. Images belong to their owners, and are utilized under personal use in this project.