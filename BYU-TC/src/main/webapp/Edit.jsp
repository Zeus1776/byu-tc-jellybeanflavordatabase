<%@page import="com.zach.JBFDMule"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%JBFDMule mule = (JBFDMule)request.getAttribute("mule");%>
<title>JellyBean Flavor Database - Edit</title>
</head>
<body>
<div id="page">
	<div id="title">
		<h1>JellyBean Flavor Database</h1>
	</div>
	<div id="body">
		<div id="editContainer">
			<form action="/BYU-TC/JBFD" method="post">
				<label>Current JellyBean Flavor: <%= mule.flavorToEdit %></label>
				<input type="text" name="flavorEdit" value="<%= mule.flavorToEdit %>"></input>
				<input type="submit" name="action" value="Save"></input>
				<input type="hidden" name="id" value=<%= mule.flavorIDToEdit %>></input>
			</form>
		</div>
	</div>
</div>
</body>
</html>