<%@page import="com.zach.JBFDMule"%>
<%@page import="com.zach.JellyBeanFlavor" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%JBFDMule mule = (JBFDMule)request.getAttribute("mule");%> <!-- PULL IN THE MULE -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300..900;1,300..900&display=swap"> <!-- GET THE GOOGLE FONT -->
<link rel="stylesheet" type="text/css" href="styles.css"> <!-- MAKE IT ALL PRETTY -->
<title>JBFD - Jelly Bean Flavor Database</title>
</head>
<body>
<div id="page">
	<div id="title">
		<h1>JellyBean Flavor Database</h1>
		<img src="images/JellybeanSmallPile.png" height="50"></img>
	</div>
	<div id="wallpaper">
		<div id="uiContainer">
			<!---------------------------- HERE IS THE ADD FUNCTIONALITY -->
			<div id="left">
				<div id="addArea">
					<form id="addForm" action="/BYU-TC/JBFD" method="post">
						<label>New JellyBean Flavor: </label>
					  	<input type="text" name="flavorToAdd" value="<%= mule.flavorToAdd %>">
					  	<input id="addButton" class="itemButton2" name="action" type="submit" value="Add">
					</form>
				</div>
			</div>
			<!---------------------------- THE MIDDLE LISTS OUT THE FLAVORS, AND LETS YOU EDIT/DELETE THEM TOO -->
			<div id="middle">
				<div id="listContainer">
					<ul id="list">
						<%
						if (mule.FlavorList.size() != 0) {
							for (JellyBeanFlavor item : mule.FlavorList) {
								%>
								<li id="li_<%= item.getFlavorID() %>">
									<form action="/BYU-TC/JBFD" method="post">
									<div class="liContainer"> <!-- THESE ARE IN DIVS TO HELP SPLIT THEM APART FROM EACH OTHER AND MAKE SOME SPACE -->
										<div class="liText">
											<label id="name_<%= item.getFlavorID() %>"><%= item.getFlavor() %></label>
											<input type="hidden" name="editFlavor" value="<%= item.getFlavor() %>" id="editName_<%= item.getFlavorID() %>"></input>
										</div>
										<div class="liButtons">
											<input type="submit" name="action" value="Save" class="save itemButton2" id="save_<%= item.getFlavorID() %>"></input>
											<input type="button" name="action" value="Cancel" class="cancel itemButton2" 
												onclick="cancelClick(<%= item.getFlavorID() %>)" id="cancel_<%= item.getFlavorID() %>"></input>
											<input type="button" name="action" value="Edit" class="edit itemButton" 
												onclick="editClick(<%= item.getFlavorID() %>)" id="edit_<%= item.getFlavorID() %>"></input>
											<input type="submit" name="action" value="Delete" class="delete itemButton" id="delete_<%= item.getFlavorID() %>"></input>
											<input type="hidden" name="id" value=<%= item.getFlavorID() %>></input>
										</div>
									</div>
									</form>
								</li>
								<%
							}
						} else {
						%>
							<li>No flavors to display</li> <!-- A SAD DAY WHEN ONE HAS NO FLAVORS TO DISPLAY -->
						<%}%>
					</ul>
				</div>
			</div>
			<div id="right">
				<img src="images/JellybeanJar2.png" width="370">
			</div>
		</div>
	</div>
	<div id="footer">
		<h4>JellyBean Flavor Database</h4>
		<h4>Copyright Zach Payne &copy;2024</h4>
		<h4>IMAGES UTILIZED FOR PERSONAL USE</h4>
	</div>
</div>

<script>

	//FANCY COLORS 
	//FLAVORS WILL RANDOMLY GET CHANGE TO A COLOR ON HOVER, THEN SLOWLY FADE AFTER - Look into matching color to flavor? Probably too much work for now	
	var colors = ["#ff89d0", "lime", "yellow", "orange", "pink", "aqua", "magenta"];
	//var colors = ["#83d0ff", "#ff89d0", "#cf7bff", "#6bf96e", "#fff165"]; // Pastel options, too Eastery in my opinion
	var listItems = document.querySelectorAll("#list li");

	listItems.forEach(function(item) {
		item.addEventListener("mouseenter", function() {
			item.style.transition = "background-color 0s";
			var randIndex = Math.floor(Math.random() * colors.length);
			item.style.backgroundColor = colors[randIndex];
		});
		item.addEventListener("mouseleave", function() {
			item.style.transition = "background-color 1s ease";
			item.style.backgroundColor = "white";
		});
	});
	
	//ADD BUTTON WILL RANDOMLY GET CHANGE TO A COLOR ON HOVER, THEN SLOWLY FADE AFTER		
	var addButton = document.getElementById("addButton");
	addButton.addEventListener("mouseenter", function() {
		addButton.style.transition = "background-color 0s";
		var randIndex = Math.floor(Math.random() * colors.length);
		addButton.style.backgroundColor = colors[randIndex];
	});
	addButton.addEventListener("mouseleave", function() {
		addButton.style.transition = "background-color 1s ease";
		addButton.style.backgroundColor = "white";
	});
	
	//------------------------------------------------------------------------

	//INLINE EDIT - Enables editing a flavor, while disabling other edits, deletes, or the add button
	function editClick(id) {
		//PREVENT DATA/WORK LOSS BY DISABLING OTHER BUTTONS DURING EDIT (Multiple edits, a delete, or an add, will erase an in-progress edit)
		var listEditButtons = document.querySelectorAll(".edit");
		listEditButtons.forEach(function(item) { item.disabled = true; });
		var listDeleteButtons = document.querySelectorAll(".delete");
		listDeleteButtons.forEach(function(item) { item.disabled = true; });
		var addButton = document.getElementById("addButton");
		addButton.disabled = true;

		//Build the ids that are needed
		var editButton = document.getElementById("edit_" + id);
        var saveButton = document.getElementById("save_" + id);
        var cancelButton = document.getElementById("cancel_" + id);
        var deleteButton = document.getElementById("delete_" + id);
        var nameField = document.getElementById("name_" + id);
        var editNameField = document.getElementById("editName_" + id);
		
        //Hide edit, delete, and name
		editButton.style.display = "none";
		deleteButton.style.display = "none";
		nameField.style.display = "none";
		
		//Show save, cancel, and text input
		saveButton.style.display = "inline-flex";
		cancelButton.style.display = "inline-flex";
		editNameField.type = "text";
	}

	//CANCEL - Reverts to default page state
	function cancelClick(id) {
		//RE-ENABLE BUTTONS POST-CANCEL
		var listEditButtons = document.querySelectorAll(".edit");
		listEditButtons.forEach(function(item) { item.disabled = false; });
		var listDeleteButtons = document.querySelectorAll(".delete");
		listDeleteButtons.forEach(function(item) { item.disabled = false; });
		var addButton = document.getElementById("addButton");
		addButton.disabled = false;
		
		//Build the ids that are needed
        var cancelButton = document.getElementById("cancel_" + id);
        var saveButton = document.getElementById("save_" + id);
        var editButton = document.getElementById("edit_" + id);
        var deleteButton = document.getElementById("delete_" + id);
        var nameField = document.getElementById("name_" + id);
        var editNameField = document.getElementById("editName_" + id);
		
      	//Hide save, cancel, and text input
		cancelButton.style.display = "none";
		saveButton.style.display = "none";
		editNameField.type = "hidden";
		
		//Show edit, delete, and name
		editButton.style.display = "inline-flex";
		deleteButton.style.display = "inline-flex";
		nameField.style.display = "inline";
		
	}
	
</script>
</body>
</html>