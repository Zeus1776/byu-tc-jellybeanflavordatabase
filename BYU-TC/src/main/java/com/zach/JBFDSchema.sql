CREATE DATABASE BYUTC;
USE BYUTC;
CREATE TABLE
  `jellybeanflavor` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `flavor` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE = InnoDB AUTO_INCREMENT = 5 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;