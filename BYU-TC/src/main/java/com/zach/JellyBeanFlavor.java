package com.zach;

public class JellyBeanFlavor {
    public JellyBeanFlavor(int flavorID, String flavor) {
        this.flavorID = flavorID;
        this.flavor = flavor;
    }

    private int flavorID;
    private String flavor;

    public int getFlavorID() {
        return flavorID;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }
}
