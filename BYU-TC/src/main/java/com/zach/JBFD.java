package com.zach;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class JBFD
 */
@WebServlet("/JBFD")
public class JBFD extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JBFDMule mule = new JBFDMule();
		String jsp = "/JBFD.jsp";
		JellyBeanDao dao = new JellyBeanDao();
		Connection connection = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BYUTC", "root", "J3llyB34n");
		
			if (request.getParameter("action") == null) {
				// DEFAULT - READS ALL FLAVORS FROM DB AND DISPLAYS ON ARRIVAL TO PAGE
				mule.FlavorList = dao.getFlavors(connection);
			} else if (request.getParameter("action").equals("Add")) {
				// CREATE - ADDS FLAVOR TO DB
				mule.flavorToAdd = request.getParameter("flavorToAdd"); // This would be a backup if the DAO goofs. 
				dao.createFlavor(mule.flavorToAdd, connection);
				mule.FlavorList = dao.getFlavors(connection); // Get updated flavor list
				mule.flavorToAdd = ""; // Clears the backup (and the add field on the jsp)
			} else if (request.getParameter("action").equals("Save")) {
				//UPDATE - EDITS FLAVOR IN DB
				dao.updateFlavor(Integer.parseInt(request.getParameter("id")), request.getParameter("editFlavor"), connection);
				mule.FlavorList = dao.getFlavors(connection); // Get updated flavor list
			} else if (request.getParameter("action").equals("Delete")) {
				//DELETE - DELETES FLAVOR FROM DB
				dao.deleteFlavor(Integer.parseInt(request.getParameter("id")), connection);
				mule.FlavorList = dao.getFlavors(connection); // Get updated flavor list
			}
			
		} catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
        	if (connection != null) {
        		try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        }
		
		// Send the mule along with its load of goodies
		request.setAttribute("mule", mule);
		request.getServletContext().getRequestDispatcher(jsp).include(request, response);
	}

}
