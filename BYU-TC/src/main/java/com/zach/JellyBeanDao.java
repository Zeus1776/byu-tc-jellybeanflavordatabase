package com.zach;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class JellyBeanDao {

   //================== CRUD ==================//
   // CREATE ---------------------------------------
    public void createFlavor(String flavor, Connection connection) throws SQLException {
        // Add entry to DB
    	String addJBFlavorSQL = 
    			"INSERT INTO jellybeanflavor (flavor) " + 
    			"VALUES (?)";
    	PreparedStatement addJBFlavorPS = connection.prepareStatement(addJBFlavorSQL);
    	addJBFlavorPS.setString(1, flavor);
    	addJBFlavorPS.executeUpdate();
    	addJBFlavorPS.close();
    }

    // READ - ALL ---------------------------------------
    public ArrayList<JellyBeanFlavor> getFlavors(Connection connection) throws SQLException {
        //get and return all entries
    	ArrayList<JellyBeanFlavor> flavorList = new ArrayList<>();
    	String getFlavorsSQL = "Select id,flavor FROM jellybeanflavor";
		PreparedStatement getFlavorsPS = connection.prepareStatement(getFlavorsSQL);
		
		ResultSet getFlavorsRS = getFlavorsPS.executeQuery();
		while (getFlavorsRS.next()) {
			int id = getFlavorsRS.getInt("id");
			String flavor = getFlavorsRS.getString("flavor");
			flavorList.add(new JellyBeanFlavor(id, flavor));
		}
		getFlavorsPS.close();
        return flavorList;
    }

    //READ - SINGLE ENTRY ----------------------------------
    public JellyBeanFlavor getFlavor(int flavorID, Connection connection) throws SQLException {
    	String getFlavorSQL = "Select flavor FROM jellybeanflavor WHERE id = ?";
		PreparedStatement getFlavorPS = connection.prepareStatement(getFlavorSQL);
		getFlavorPS.setInt(1, flavorID);
		ResultSet getFlavorRS = getFlavorPS.executeQuery();
		String flavor = "";
		if (getFlavorRS.next()) {
			flavor = getFlavorRS.getString("flavor");
		}
		getFlavorPS.close();
        return new JellyBeanFlavor(flavorID, flavor);
    }

    //UPDATE ---------------------------------------
    public void updateFlavor(int flavorID, String flavor, Connection connection) throws SQLException {
    	String updateJBFlavorSQL = 
    			"UPDATE jellybeanflavor " + 
    			"SET flavor = ? " +
    			"WHERE id = ?";
    	PreparedStatement updateJBFlavorPS = connection.prepareStatement(updateJBFlavorSQL);
    	updateJBFlavorPS.setString(1,  flavor);
    	updateJBFlavorPS.setInt(2, flavorID);
    	updateJBFlavorPS.executeUpdate();
    	updateJBFlavorPS.close();
    }

    //DELETE ---------------------------------------
    public void deleteFlavor(int flavorID, Connection connection) throws SQLException {
    	String deleteJBFlavorSQL = 
    			"DELETE FROM jellybeanflavor " + 
    			"WHERE id = ?";
    	PreparedStatement deleteJBFlavorPS = connection.prepareStatement(deleteJBFlavorSQL);
    	deleteJBFlavorPS.setInt(1, flavorID);
    	deleteJBFlavorPS.executeUpdate();
    	deleteJBFlavorPS.close();
    }
}